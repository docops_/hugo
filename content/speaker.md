# Speaking

Welcome to my speaking engagements page! I'm passionate about sharing my insights and solutions as a technical writer. Below you'll find a list of my past speaking engagements at conferences, meetups, and podcasts.

I am a seasoned technical writer with a deep commitment to the field. As a speaker, I bring my expertise to professional conferences, meetups, and podcasts. I believe in the power of knowledge sharing and strive to inspire and educate my audience.

![Fear of Speaking](/static/speaking.gif)

{{< creadlybadge >}}

## Past Conferences

### [IT Knowledge Management meetup, August 1 2024, Online](https://be.gurtam.com/meetups/01-08-2024)
- Topic: Preparing internal knowledge for AI: Making content fit for LLMs and RAG, yet human-readable
{{< youtube oXaV9rU3NM4 >}}

---

### [apidays Paris, December 5-8 2023](???)
- Topic: Let's Bring Science Into API Documentation
No recording available.

---

### [API The Docs Amsterdam, June 6-7, 2023](https://apithedocs.org/amsterdam-2023/lana-novikova)
- Topic: Using Domain-Specific Languages (DSLs) to Document API — the Kotlin DSL story
No recording available.

---

### [Write the docs Australia, December 2022, Online](https://youtu.be/TQu7vw90tME)
- Topic: Let's bring science into technical writing
{{< youtube TQu7vw90tME >}}

---

### [Podlodka TL Crew 2020, September 2020, Online](https://youtu.be/ge2UV2duygE)
- Workshops: Knowledge Management in Minimalist and How to Write Clear Technical Texts

---

### [Rivelty 2020 Conference of Internal Communications and Corporate Portals, February 2020, Moscow](http://conf.rivelty.ru/)
- Topic: Knowledge Management for Internal Communications

---

### [Saint TeamLead Conf 2018, St. Petersburg](https://youtu.be/ITCUNTovW3M)
- Topic: Competency matrix as a tool for team leads (with Konstantin Kaftan)

---

### [SECR 2017, November 2017, St. Petersburg](https://www.youtube.com/watch?v=GkwJbpcFYu0&feature=youtu.be)
- Topic: Confluence and collaborative documentation - opportunities and pitfalls

## Meetups

- [How we tried to reconcile OpenAPI 3.0 and the custom DRF framework, December 2019, Mini Hyperbaton](https://www.youtube.com/watch?v=FBIbEBK1U1w&fbclid=IwAR3MxXQdagzcKoN7UgZJpAUhV9SDqxN9Wrez8qtdunHfB59HteN_ZB5rilM)
- [Confluence vs Sphinx, July 2019, Write the docs Moscow meetup](https://youtu.be/5N3zyvXB6Jg?t=2455&fbclid=IwAR08bYVloQYJWXYxjBObpFsVS5SOFHqyc9BEbGlMOM5lxaf0Hx5k3o4h3iA)
- [Knowledge management with competence matrix, October 14, Write the docs Moscow meetup](https://www.youtube.com/watch?v=iQMFVTOo7Ug)

## Podcasts

- [Podlodka Podcast, Knowledge management](https://soundcloud.com/podlodka/podlodka-103-upravlenie-znaniyami?fbclid=IwAR3V8kXSEhJDa2QGi5gZuWBFf_29JKszYufmDw7K3Bqg6o1FdkAqcS1FLAQ)
- [The Art of Programming](https://theartofprogramming.podbean.com/e/191-%d1%83%d0%bd%d0%b0%d1%81%d0%bd%d0%b5-%d0%b1%d1%83%d0%b4%d0%b5%d1%82-%d0%b0%d0%bb%d1%8c%d1%82%d0%b5%d1%80%d0%bd%d0%b0%d1%82%d0%b8%d0%b2/?fbclid=IwAR1DrwqCoMojbN1NcPzW5vWFfpRrN50DbMm9KMQafoCuxph1mcKsV5GfejM)

## Upcoming Talks

- [OpenSource Summit Vienna, September 16-18, 2024 —
Documentation Templates: A Helpful Aid or an Obstacle](https://osseu2024.sched.com/event/1ej5q?iframe=no)

Stay tuned for my upcoming talks! More information will be added soon.

If you're interested in collaboration or have any questions, feel free to contact me at [svetlnovikova@gmail.com](mailto:svetlnovikova@gmail.com).