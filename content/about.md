# About Me

Hello there! I'm Lana, a dedicated technical writer, docops enthusiast, and a specialist in knowledge management.

![Me](/static/img_square_260x260.png)

## Expertise

Over the past 8+ years, I've immersed myself in the world of documentation. My focus lies in crafting comprehensive documentation for developers, specializing in API documentation, building internal knowledge bases, and launching developer portals.

## Tools of Choice

I'm a firm advocate of efficient documentation practices. I harness the power of tools like [Sphinx Doc](http://www.sphinx-doc.org/en/master/), [Hugo](https://gohugo.io/), and embrace the docs-as-code approach. I've honed my skills in creating documentation publication pipelines tailored for diverse needs.

## Community Engagement

Being an active member of the tech writing community, particularly within the Write the Docs and The Good Docs, I'm passionate about sharing insights and knowledge. You'll often find me speaking at meetups and conferences, contributing to the collective growth of our field.

## Program Committee Involvement

I'm proud to serve as a member of the Program Committee for the pioneering Russian conference dedicated to knowledge management in the IT realm – [KnowledgeConf](https://knowledgeconf.ru/).

## Sharing Ideas

I love to share my thoughts and expertise. You can find my articles on the Russian IT platform Habr, where I delve into various topics related to our ever-evolving industry: [My Articles on Habr](https://habr.com/ru/users/nerazzgadannaya/posts/).

## Let's Connect

I'm excited to connect with fellow professionals, collaborate, and explore new horizons. Feel free to reach out to me at [svetlnovikova@gmail.com](mailto:svetlnovikova@gmail.com).
