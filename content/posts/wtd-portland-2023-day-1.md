---
title: Write the Docs 2023. Day 1 Notes
tags: [notes, conference, Write The Docs]
weight: 1
comments: false
showToc: true
TocOpen: false
---

Here are my notes from the Day 1 at WTD Portland 2023. 

## Ryan Young - Is it (layer) cake: Thinking in content levels

- They have their own framework for defining information architecture, which they use to make sure every user land on the type of content they need to solve their issue. 
- He have used a perfect metaphor of information being a mall, and docs being like a map of the mall. 

Here are some problems he defined they had before, the fun part is that they are all based on a real feebback. 

![](/static/stripe-feedback.png)

Problems:

- Wrong doc scope - too broad too specific
- To many choices
- Too much information to utilize
- Too technical 
- Not clear where to start and where to go next

So what they've done to change the situation:

- Give a user not only high-level, conceptual information, but some mid-level so they can continue with the decision. You still orienting users but on another level of abstraction. It is sothing like a bridge, lower layer than an overview with more depth
- Connecting docs with actual UI (immersive docs embedding fuctionality from UI, docs that back up the hints in the UI in text)
- Create dedicated docs for no code users
- Provide context for every option and piece of integration
- Presenting options but not as a list but as a guidance how to reason between them

As a result, they've got a layered cake of docs, where high-level is used for orientation, middle — for decision, and low — for implementation.

![A layered cake of docs framework](/static/layered-cake.png)

# Adam Altman - From resistance to adoption: navigating the challenges of docs-like-code 

- Adam's talk is dedicated to docs-as-code and he intended to guide everyone who is scared and lost throuth the process of implementation
- Popular misconception oabout docs-as-code — you need a Computer science degree to use it. Rather thousands of conversations
- Docs-as-code is not about SSGs or git, it is about a lot of communications, collaboration, convicting people to change their workflows

![](/static/a-b-c-of-docs.png)

- Three steps:
  - Assess — Docs-as-code is not appropriate for every company and use case 
  - Build — try in 30min, evaluate the migration cost, test with a non-techie and adjust your estimations 
  - Convince your boss and yourself — find the transformation that make sense for your company business, explain in simple words, appraise value, pre-handle objections.
- Here is a ready-to-use template for pre-handling the possible objections:

![](/static/prehandle-objections.png)

- Here are some possible objections you can face:
  - It's not good at handling content reuse
  - It's unable to work with visual content
  - It lacks support for real-time collaboration
  - It can't handle complex formatting
  - No ability to integrate with other compamy tools
  - concerns about using git and Markdown

## Linda Ikechukwu - Myths, Mistakes, and Missed Opportunities: Lessons learned from a year of interviewing technical writers.

Linda Ikechukwu summarized her experience during interviews with people from the tech comm field and here are some lessons learned:
- Submit a style guide formatted doc
- Use metrics to show impact you've made
- Use precise verbs to describe your tasks and achievements
- If you have no portfolio or writing samples, search through GitHub and contribute to open source projects
- You don't need to be coder, but focus on one programming language, learn just enough to build a small app
- You don't become tech writer by getting the job, you become it long before

Also, Linda during her talk spotted a problem: Bootcamps, schools and IT courses are mainly focused on coding, not technical communications, so many people even didn't know they can get paid for writing in tech.

Follow Linda at: https://www.everythingtechnicalwriting.com/. 

## Lightning talks

### Jewel How can emojies be read differently

- We care about a lot of people who are reading our content - cultures, backgrounds, age. 
- The background can influence even the way you're reading the emojis
  - For example, folded hands can mean thank you, pray, and congrats (high-five). 
  - Smile with a teeth - can be a smile for one or "I am gonna bite you" for another.
- Think on how is this gonna read depending on the culture they're from
- When you go to localizers  - mind, they may need to translate the emojies too
- Also, mind that unline text and letters emojies have different varieties on Windows, Android, macOS. So check  all the variants.
- Double check what is the label for this emojies, it usually helps

### Julia CARs for writers

Julia taught us to dedicate time to ourselves with CARs which is an acronym for Controlled Articular Rotations. Do simple stretches during the work day.

### Ryan Maclene Stop saying just be logical 

- Human brains and behaviour is now strictly logical it can be squishi
- We all receive stimuls and get the reactions back
- Stimules can be anything - news, challendges, requests, concerns
- So don't write for machines and code, write for humans

![](/static/dont-be-logical.png)

### Ashley Gordon – Tech Writing Internship

- Ashlee Gordon, tech writer at LumaHealth, has told how to convince the company to hire an intern. 
- Some arguments include: 
  - getting a new, fresh point of view from the person of another age and background
  - tell your boss what interns will be doing, always have a precise plan for the internship, what they'll learn and do
  - also tell how it will offload the team, not add the workload

### Matthew Shaver – Lego your docs

Some lessons from building a Lego castle with his daughter: 
- Keep the result in sight (like Lego keeps it on the box)
-  Get your customers onto your project, let them along the way
- Split into chunks (like lego splitting into bags)
- Define target audience (like lego defining the age on the boxes)

And the last but not least — clean up all the details left after or you'll step on it. 

## Daniel Murphey - Release-ready Docs: How a 2-person team keeps a help center consistently accurate

- By release readyness thet mean — doing the advance work needed to make all their docs accurate and complete withon 24h of a release
- Why it is important:
  - Sanity (no last minute changes, no backlog)
  - Simplicity (limited number of edits in a framed time)
  - Customer trust (up-to-date docs)
  - Team support (support team contribute to the success)
  - AI (works better if you present it an up-to-date information)
- Tech writers don't work with users, they rely heavily on support and customer success team
- How they achieve it in general:
  - Well-documented processes
  - Planning
  - Communication
- So how the process look like:
  - Step 1 Awareness - roadmap, slack channels, meeting and tpuch points, UX review. Continual refinement
  - Step 2 Scoping: They do an initial gut check. Define which new articles they need and which nedd to be updated and the change type (text or image). Change issues - which changes affect each article and come up with a simple to do list with some custom task fields for change type and release issues. 
    - Normal release is usually 20-50 articles = start to prepare 30 days before release.
    - Big release is 50+ articles, starts ASAP.
    - A piece of advice: Bucket changes into what should be done on a pre-stage and what — on a post-.
  - Step 3. Drafting. Do the preparation to help future self. Some opportunities for combos here: check if you have planned audits or submitted feedbacks for these articles and do the work. 
    - Futire-proofing. Some tips on how you can save time for your future self:
      - Simple screenshots — showing only the part that is needed for comprehension so that they don't need to retake them frequently
      - Future-proofing — adjust wording for future changes, same for dates on the screenshots
      - Safe exclusion - removed what will be removed in advance. Use with caution!
  - Step 4. Release. Always announce within the team first, publish all edits and what's new, localise changes.

Some more takeaways:

![](/static/takeaways.png)

## Sarah Greene - Did You Read the SOP? Procedure Writing for the Laboratory

- Documentarians and laboratorians are somewhat related — we both love constsrency, acciracy, and clarity.
- SOPs are crucial, they should be repeatable and detailed.
- Some advices for non-techwriters on writing procedures:
  - Map out the process first with flow charts or drawings
  - Then you can go with the flow and throw as much text as possible, edit later
  - Use should, could - precise verbs
  - Utlize colleagues to test the procedure on them
  - USe it as an opportunity to improve workflows

![](/static//flow-chart.png)

And my favorite part of the talk!

![](/static/everybody.png)

## Dan Grabski – Zen And The Art Of Automanually Creating API Documentation – An Inquiry Into Process

- The title originated from the book [Zen and the Art of Motorcycle Maintenance: An Inquiry Into Values](https://en.wikipedia.org/wiki/Zen_and_the_Art_of_Motorcycle_Maintenance) about a summer motorcycle trip undertaken by a father and his son.
- He proposed two principles you can utilize for your API docs to keep them sane and consistent:
  - Manual before automatic: In the world where many people want as much automation as possible they practice Manual before automatic approach. It helps to catch errors and make improvements into the automated part. 
  - Working with humans: interpersonal improvement, consolidate efforts, reduce conflict
