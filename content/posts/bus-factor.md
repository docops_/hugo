---
title: How to reveal and calculate the bus factor on a project
tags: [knowledge management, bus factor]
---

These days many project leads talk about the bus factor, the truck factor, or the brick factor,
which is **the number of developers, engineers on a project that can get hit by a bus/truck/brick,
causing the project to stop**.

![](/static/bus-factor2.jpeg)

To put it even more simple it is a measure of the concentration of information about a system, a code a project.
The most critical bus factor is 1.

The term, of course, is not new; it appeared in business management back in 1998.

"Bus" can be anything, such as a vacation, a layoff, a maternity leave, a meditation or a retreat on Bali, or even going on a binge, it's figurative.
But it is much harder to understand where these places in the system, where the bus factor lies, to reduce it.

![](/static/bus-factor.jpeg)

## How to calculate the bus factor?

Let's imagine you have 30 people on a project that does web development,
it has an API, a frontend and design, no details.
5 people develop the API, 10 people develop the frontend and another 5 do the design, your bus factor is 5.

## Potential signals of having a bus factor

- You have developers whose code you don't review. Why? He's a cool dude, it doesn't make sense to review him, he writes his own style. Then no one will ever understand this code base.
- A special case of the previous one, is a "code ownership" practice, that is in general not bad, but can lead to a situation when only one preson knows a particular part of code well. Even if you stick to the "code ownership", allow other collegues to ask questions and to challendge each other.
- You have no uniform naming of classes, variables, module arrangement structure in the project, no requirements for documented code and you don't check it, either manually or automatically.
- The CI/CD on the project was set up by one person, everyone asks him to add, change, but no one goes into details.
- You have chunks of code commented out a long time ago with no understanding of why this is the case or in what situations you can't in any way uncomment it.
- The documentation and knowledge base is stored in private spaces, or worse, on local machines that can get formatted.

## Can it be detected automatically?

Yes, but it will only be diagnostic, the team leader will have to dig deeper manually.
For example, there's [a tool like this](https://github.com/yamikuronue/BusFactor) for Git/HG repositories that lets you find files where one person
is the main line-of-code contributor.




